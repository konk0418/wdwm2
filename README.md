# WDWM2

A rewrite of my (Will's) build of DWM, with cleaner code, easier patching, and less bugs!

Credit to Luke Smith at https://lukesmith.xyz for the dwmblocks scripts

## What's different?
* WDWM2 is based on DWM 6.4, as compared to DWM 6.2 on WDWM

* Cleaner code: I had been patching and maintaining WDWM For about 3 years, and it got to the point that even doing a basic patch was more work than it was worth. 90 percent of the patches in WDWM were unused. For my sanity, I only applied the patches that I used, (with the replacement of vanity gaps instead of ru-gaps), giving me the same UX, but with much cleaner and more comprehensible code.

## Patches Used:
* Autostart

* Cfacts

* Vanitygaps

* Colorbar

* Hidevacanttags

* Scratchpad

* Swallow

* TapResize

## Dependencies:
* ST

* Nitrogen

* Xbindkeys

* Pipewire and related utils (Pipewire-pulse, wireplumber)

* Nerd Fonts

## Installation:

* Clone the repo (slock is optional)

* Run `make install clean` for dwm, dmenu, and dwmblocks, and make the `.dwm/autostart.sh` script as well as the statusbar scripts located in `.dwm/statusbar/` executable with `chmod`

* Login to dwm using `exec dwm` in your `.xinitrc` or create a desktop file for it if you use a graphical login screen

## Keybinds:
* `ALT + ENTER`: Terminal (ST)

* `ALT + SPACE`: dmenu

* `ALT + SHIFT + ENTER`: Promote window from stack to master

* `ALT + SHIFT + SPACE`: Toggle if a window is floating or not

* `ALT + U`: Center master mode

* `ALT + O`: Center floating mode

* `ALT + F`: Floating mode

* `ALT + W`: firefox

* `ALT + Q`: scratchpad (uses ST by default)

* `ALT + SHIFT + C`: Kill (logout of) dwm

* `ALT + SHIFT Q`: Kill focused window

* `ALT + LEFT CLICK`: Move floating window

* `ALT + RIGHT CLICK`: Resize floating window
